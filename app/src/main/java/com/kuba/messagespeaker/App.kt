package com.kuba.messagespeaker

import android.app.Application
import android.net.Uri
import android.os.Handler
import com.kuba.messagespeaker.observers.OutgoingMessagesObserver
import com.kuba.messagespeaker.prefs.SynchronizeResult_
import com.kuba.provider.RealmProvider
import org.androidannotations.annotations.EApplication
import org.androidannotations.annotations.sharedpreferences.Pref

@EApplication
open class App : Application() {
    override fun onCreate() {
        RealmProvider.init(applicationContext)
        contentResolver.registerContentObserver(Uri.parse("content://sms"), true, OutgoingMessagesObserver(Handler(), contentResolver))
        super.onCreate()
    }
}