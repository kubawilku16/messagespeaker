package com.kuba.messagespeaker.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity<P : BasePresenter<V>, V : BaseView> : AppCompatActivity() {
    protected var presenter: P? = null

    abstract fun createPresenter(): P

    override fun onCreate(savedInstanceState: Bundle?) {
        presenter = createPresenter()
        super.onCreate(savedInstanceState)
    }

}