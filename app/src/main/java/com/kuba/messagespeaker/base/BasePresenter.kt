package com.kuba.messagespeaker.base

abstract class BasePresenter<V : BaseView> {
    protected var view: V? = null
    fun bind(view: V) {
        this.view = view
    }
}