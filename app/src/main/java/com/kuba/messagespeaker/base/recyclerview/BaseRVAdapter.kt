package com.kuba.messagespeaker.base.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import java.util.*

open class BaseRVAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items: MutableList<BaseRVItem>? = LinkedList()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = items?.get(position)!!.bind(holder)

    override fun getItemViewType(position: Int): Int = items?.get(position)!!.getViewType()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder? {
        items?.forEach { item ->
            if (item.getViewType() == viewType) {
                return item.getViewHolder(parent)
            }
        }
        return null
    }

    override fun getItemCount(): Int = items?.size ?: 0

    fun addItem(newItem: BaseRVItem) {
        items?.add(newItem)
    }

    fun addItemAtPosition(newItem: BaseRVItem, position: Int) {
        items?.add(position, newItem)
        notifyItemInserted(position)
    }

    fun clearItems() {
        notifyItemRangeRemoved(0, itemCount)
        items?.clear()
    }

    fun clearItemAtPosition(position: Int) {
        notifyItemRemoved(position)
        items?.removeAt(position)
    }

    fun getItem(position: Int): BaseRVItem? =
            if (items == null) null else items?.get(position)

}