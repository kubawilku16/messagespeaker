package com.kuba.messagespeaker.base.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

abstract class BaseRVItem {
    abstract fun getViewType(): Int

    abstract fun getViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    abstract fun bind(holder: RecyclerView.ViewHolder)
}