package com.kuba.messagespeaker.conversation

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context

import android.content.Intent
import android.content.IntentFilter
import android.provider.Telephony
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSmoothScroller
import com.kuba.messagespeaker.R
import com.kuba.messagespeaker.base.BaseActivity
import com.kuba.messagespeaker.base.BaseView
import com.kuba.messagespeaker.model.ConversationModel
import com.kuba.messagespeaker.model.MessageModel
import com.kuba.messagespeaker.recognizer.Recognizer
import com.kuba.messagespeaker.utils.HeaderItemDecoration
import com.kuba.messagespeaker.utils.TextChangeListener
import com.tonicartos.superslim.LayoutManager
import kotlinx.android.synthetic.main.activity_conversation.*
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.Extra
import org.jetbrains.anko.alert
@SuppressLint("Registered")
@EActivity(R.layout.activity_conversation)
open class ConversationActivity : BaseActivity<ConversationPresenter, ConversationActivity>(), BaseView {

    @Extra
    @JvmField
    var id: String? = ""

    @Extra
    @JvmField
    var title: String? = ""

    var anythingSent: Boolean = false

    private var adapter: ConversationAdapter? = null
    private var recognizer: Recognizer? = null
    private var receiver = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            Telephony.Sms.Intents.getMessagesFromIntent(intent)
                    .forEach { smsMessage -> presenter?.receiveMessage(smsMessage)}
        }
    }

    override fun createPresenter(): ConversationPresenter = ConversationPresenter()

    @AfterViews
    fun onInit() {
        presenter?.bind(this)
        presenter?.onInit()
    }

    @Click(R.id.conversation_clear)
    fun onClearClick() {
        presenter?.onClearClick()
    }

    @Click(R.id.conversation_speak)
    fun onSpeakClick() {
        presenter?.onSpeakClick()
    }

    @Click(R.id.conversation_send)
    fun onSendClick() {
        presenter?.onSendClick()
    }

    fun initRecycler() {
        conversation_recycler.setHasFixedSize(true)
        conversation_recycler.layoutManager = LinearLayoutManager(applicationContext)
        conversation_recycler.adapter = adapter
        conversation_recycler.addItemDecoration(HeaderItemDecoration(conversation_recycler, adapter as HeaderItemDecoration.StickyHeaderInterface))
    }

    fun setTitle() {
        conversation_title.text = title
    }

    fun initAdapter() {
        adapter = ConversationAdapter()
    }


    fun initTextChangeListener() {
        conversation_edit_text.addTextChangedListener(TextChangeListener({ text -> presenter?.onTextChange(text) }))
    }

    fun initRecognizer() {
        recognizer = Recognizer(applicationContext,
                { state -> onRecognizerStateChange(state) },
                { result -> presenter?.appendNewMessage(result) }).create()
    }

    private fun onRecognizerStateChange(state: Boolean) {
        if (state) {
            conversation_speak.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.microphone_on))
        } else {
            conversation_speak.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.microphone_off))
        }
    }

    fun onTextChange(isEmpty: Boolean) {
        if (isEmpty) {
            conversation_clear.isClickable = false
            conversation_clear.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.clear_off))
            conversation_send.isClickable = false
            conversation_send.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.send_off))
        } else {
            conversation_clear.isClickable = true
            conversation_clear.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.clear_on))
            conversation_send.isClickable = true
            conversation_send.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.send_on))
        }
    }

    fun loadData(model: ConversationModel?) {
        val scroller = LinearSmoothScroller(applicationContext)
        scroller.targetPosition = 0
        conversation_recycler.layoutManager.startSmoothScroll(scroller)
        adapter?.loadData(model)
    }

    fun clearText() = conversation_edit_text.text.clear()

    fun runOrCancelSpeaker() {
        recognizer?.onSpeakClick()
    }

    fun getContent(): String = conversation_edit_text.text.toString()

    fun addNewMessageToList(messageModel: MessageModel) = adapter?.addNewItem(messageModel, "")

    fun showErrorMessage() =
            alert(getString(R.string.send_error), getString(R.string.error)) {
                positiveButton(getString(R.string.again)) { presenter?.onSendClick() }
                negativeButton(getString(R.string.cancel)) { }
                onCancel { }
            }.show()

    fun clearTextFocus() {
        conversation_edit_text.clearFocus()
        conversation_edit_text.isCursorVisible = false
    }

    override fun onBackPressed() {
        if (anythingSent) {
            setResult(1234, Intent().putExtra("id", id))
        } else {
            setResult(1234, Intent().putExtra("id", ""))
        }
        super.onBackPressed()
    }

    fun moveListToTop() {
        conversation_recycler.scrollToPosition(0)
    }

    fun appendMessage(message: String) {
        conversation_edit_text.text.append(" " + message)
    }

    fun initReceiver() {
        registerReceiver(receiver, IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION))
    }

    override fun onDestroy() {
        unregisterReceiver(receiver)
        super.onDestroy()
    }
}
