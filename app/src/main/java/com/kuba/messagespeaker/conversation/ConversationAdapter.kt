package com.kuba.messagespeaker.conversation

import android.view.View
import com.kuba.messagespeaker.base.recyclerview.BaseRVAdapter
import com.kuba.messagespeaker.model.ConversationModel
import com.kuba.messagespeaker.model.MessageModel
import com.kuba.messagespeaker.utils.HeaderItemDecoration
import com.kuba.messagespeaker.utils.SectionItem

class ConversationAdapter : BaseRVAdapter(), HeaderItemDecoration.StickyHeaderInterface {
    override fun getHeaderPositionForItem(itemPosition: Int): Int = (getItem(itemPosition) as SectionItem).getSectionIndex()


    override fun getHeaderLayout(headerPosition: Int): Int = getItem(headerPosition)!!.getViewType()

    override fun bindHeaderData(header: View, headerPosition: Int) =
            getItem(headerPosition)!!.bind(HeaderItem.ViewHolder(header))

    override fun isHeader(itemPosition: Int): Boolean = getItem(itemPosition) is HeaderItem

    fun loadData(model: ConversationModel?) {
        var section = 0
        model!!.days.forEach { date ->
            run {
                addItem(HeaderItem(date, section))
                model.messagesPerDay[date]!!.forEach { message -> addItem(MessageItem(message, model.photoUri, section)) }
                section = itemCount
            }
        }
        notifyDataSetChanged()
    }

    fun addNewItem(messageModel: MessageModel, uri: String) {
        if ((getItem(0) as HeaderItem).date == messageModel.date) {
            upgradeOtherSections(1)
            addItemAtPosition(MessageItem(messageModel, uri, 0), 1)
            notifyItemInserted(1)
        } else {
            upgradeOtherSections(2)
            addItemAtPosition(HeaderItem(messageModel.date, 0), 0)
            notifyItemInserted(0)
            addItemAtPosition(MessageItem(messageModel, uri, 0), 1)
            notifyItemInserted(1)
        }
    }

    private fun upgradeOtherSections(count: Int) {
        var id = itemCount - 1
        while (id >= 0) {
            (getItem(id) as SectionItem).incrementSection(count)
            id--
        }
    }
}