package com.kuba.messagespeaker.conversation

import android.telephony.SmsMessage
import android.text.TextUtils
import android.util.Log
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.kuba.messagespeaker.base.BasePresenter
import com.kuba.messagespeaker.interactor.SenderInteractor
import com.kuba.messagespeaker.interactor.get.GetConversationByIdInteractor
import com.kuba.messagespeaker.model.ConversationModel
import com.kuba.messagespeaker.model.MessageModel
import com.kuba.messagespeaker.utils.MSTimeUtils
import java.text.SimpleDateFormat
import java.util.*

class ConversationPresenter : BasePresenter<ConversationActivity>() {
    private var number: String = ""

    fun onInit() {
        view?.initTextChangeListener()
        view?.setTitle()
        view?.initRecognizer()
        view?.initAdapter()
        view?.initRecycler()
        getMessages()
    }

    private fun getMessages() {
        GetConversationByIdInteractor(view?.id!!).execute().subscribe({ model -> onNext(model) }, { throwable -> onError(throwable) })
    }

    private fun onNext(model: ConversationModel?) {
        number = model?.phoneNumber ?: ""
        view?.initReceiver()
        view?.loadData(model)
    }

    private fun onError(throwable: Throwable) {
        Log.e(javaClass.simpleName, throwable.message, throwable)
    }

    fun onTextChange(text: String) {
        view?.onTextChange(TextUtils.isEmpty(text))
    }

    fun onClearClick() = view?.clearText()

    fun onSpeakClick() = view?.runOrCancelSpeaker()

    fun onSendClick() {
        if (!TextUtils.isEmpty(number)) {
            view?.clearTextFocus()
            SenderInteractor(number, view?.getContent() ?: "").execute()
                    .subscribe({ onSuccessSend() }, { throwable -> onErrorSend(throwable) })
        }
    }

    private fun onSuccessSend() {
        view?.anythingSent = true
        view?.addNewMessageToList(MessageModel(view?.getContent()!!, 2,
                SimpleDateFormat(MSTimeUtils.MESSAGE_TIME_PATTERN, Locale.getDefault()).format(Date(System.currentTimeMillis())),
                SimpleDateFormat(MSTimeUtils.HEADER_DATE_PATTERN, Locale.getDefault()).format(Date(System.currentTimeMillis()))))
        view?.moveListToTop()
        view?.clearText()
    }

    private fun onErrorSend(throwable: Throwable) {
        Log.e(javaClass.simpleName, throwable.message, throwable)
        view?.showErrorMessage()
    }

    fun appendNewMessage(message: String?) {
        if (!TextUtils.isEmpty(message)) {
            view?.appendMessage(message!!.capitalize())
        }
    }

    fun receiveMessage(smsMessage: SmsMessage) {
        val number = getNumber(smsMessage.originatingAddress)
        if (number == this.number) {
            view?.addNewMessageToList(MessageModel(smsMessage.messageBody, 1,
                    SimpleDateFormat(MSTimeUtils.MESSAGE_TIME_PATTERN, Locale.getDefault()).format(Date(System.currentTimeMillis())),
                    SimpleDateFormat(MSTimeUtils.HEADER_DATE_PATTERN, Locale.getDefault()).format(Date(System.currentTimeMillis()))))
        }
    }


    private fun getNumber(number: String): String =
            try {
                PhoneNumberUtil.getInstance().parse(number, Locale.getDefault().country).nationalNumber.toString()
            } catch (e: Exception) {
                e.printStackTrace()
                number
            }
}