package com.kuba.messagespeaker.conversation

import android.graphics.Paint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.kuba.messagespeaker.R
import com.kuba.messagespeaker.base.recyclerview.BaseRVItem
import com.kuba.messagespeaker.utils.SectionItem
import org.jetbrains.anko.find

class HeaderItem(val date: String, private var section: Int) : BaseRVItem(), SectionItem{
    override fun getSectionIndex(): Int = section

    override fun incrementSection(count : Int) {
        section += count
    }

    override fun getViewType(): Int = R.layout.item_message_header

    override fun getViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(getViewType(), parent, false))

    override fun bind(holder: RecyclerView.ViewHolder) {
        holder as ViewHolder
        holder.date.text = date
        holder.date.paintFlags = Paint.UNDERLINE_TEXT_FLAG
    }

    class ViewHolder(viewItem: View) : RecyclerView.ViewHolder(viewItem) {
        var date : TextView = itemView.find(R.id.message_header_date)
    }
}