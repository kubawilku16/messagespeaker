package com.kuba.messagespeaker.conversation

import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.kuba.messagespeaker.R
import com.kuba.messagespeaker.base.recyclerview.BaseRVItem
import com.kuba.messagespeaker.model.MessageModel
import com.kuba.messagespeaker.utils.GlideApp
import com.kuba.messagespeaker.utils.SectionItem
import org.jetbrains.anko.find

class MessageItem(val model: MessageModel, private val photoUri: String, var section: Int) : BaseRVItem(), SectionItem {
    override fun getSectionIndex(): Int = section

    override fun incrementSection(count : Int) {
        section += count
    }

    override fun getViewType(): Int = R.layout.item_message

    override fun getViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(getViewType(), parent, false))

    override fun bind(holder: RecyclerView.ViewHolder) {
        holder as ViewHolder
        if (model.type == 2) {
            holder.leftView.visibility = INVISIBLE
            holder.rightView.visibility = VISIBLE
            holder.rightContent.text = model.content
            holder.rightHour.text = model.hour
            holder.rightPhoto.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, R.drawable.avatar_placeholder))
        } else {
            holder.rightView.visibility = INVISIBLE
            holder.leftView.visibility = VISIBLE
            holder.leftContent.text = model.content
            holder.leftHour.text = model.hour
            GlideApp.with(holder.itemView.context).load(Uri.parse(photoUri)).placeholder(R.drawable.avatar_placeholder).into(holder.leftPhoto)
        }
    }

    private class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var leftView: View = itemView.find(R.id.message_left)
        var rightView: View = itemView.find(R.id.message_right)
        var leftPhoto: ImageView = itemView.find(R.id.message_left_photo)
        var leftContent: TextView = itemView.find(R.id.message_left_content)
        var rightPhoto: ImageView = itemView.find(R.id.message_right_photo)
        var rightContent: TextView = itemView.find(R.id.message_right_content)
        var leftHour: TextView = itemView.find(R.id.message_left_hour)
        var rightHour: TextView = itemView.find(R.id.message_right_hour)
    }
}