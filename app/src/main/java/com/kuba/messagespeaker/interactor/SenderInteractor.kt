package com.kuba.messagespeaker.interactor

import android.telephony.SmsManager
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SenderInteractor(private val number: String,
                       private val content: String) {
    fun execute(): Completable = Completable.fromCallable { sendMessage() }
            .compose { upstream ->
                upstream.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
            }

    private fun sendMessage() = SmsManager.getDefault().sendTextMessage(number, null, content, null, null)
}