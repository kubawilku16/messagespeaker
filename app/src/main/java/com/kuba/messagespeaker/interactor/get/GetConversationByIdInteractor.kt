package com.kuba.messagespeaker.interactor.get

import com.kuba.entities.ContactRealm
import com.kuba.entities.ConversationRealm
import com.kuba.entities.MessageRealm
import com.kuba.messagespeaker.model.ConversationModel
import com.kuba.messagespeaker.model.MessageModel
import com.kuba.messagespeaker.utils.MSTimeUtils
import com.kuba.provider.RealmProvider
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.RealmList
import java.text.SimpleDateFormat
import java.util.*

class GetConversationByIdInteractor(val id: String) {
    fun execute(): Observable<ConversationModel> = Observable.defer { getModel() }
            .compose { upstream ->
                upstream.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
            }

    private fun getModel(): Observable<ConversationModel> {
        return Observable.just(convertToModel(RealmProvider.findById(ConversationRealm::class.java, "id", id), RealmProvider.findById(ContactRealm::class.java, "id", id)))
    }

    private fun convertToModel(conversation: ConversationRealm?, contact: ContactRealm?): ConversationModel? {
        return if (conversation == null || contact == null) {
            null
        } else {
            val messagesMap = convertMessages(conversation.messages)
            ConversationModel(id = contact.id,
                    name = contact.name,
                    messagesPerDay = messagesMap,
                    photoUri = contact.photoUri,
                    phoneNumber = contact.number,
                    days = getSortedDays(messagesMap.keys))
        }
    }

    private fun getSortedDays(keys: MutableSet<String>): MutableList<String> =
            keys.sortedByDescending { key -> SimpleDateFormat(MSTimeUtils.HEADER_DATE_PATTERN, Locale.getDefault()).parse(key) }.toMutableList()

    private fun convertMessages(messages: RealmList<MessageRealm>): MutableMap<String, MutableList<MessageModel>> {
        val result: MutableMap<String, MutableList<MessageModel>> = HashMap()
        messages.forEach { message ->
            run {
                val date: String = SimpleDateFormat(MSTimeUtils.HEADER_DATE_PATTERN, Locale.getDefault()).format(Date(message.date))
                val hour: String = SimpleDateFormat(MSTimeUtils.MESSAGE_TIME_PATTERN, Locale.getDefault()).format(Date(message.date))
                @Suppress("IMPLICIT_CAST_TO_ANY")
                if (result.containsKey(date)) {
                    result[date]!!.add(MessageModel(message.content, message.type, hour, date))
                } else {
                    val list: MutableList<MessageModel> = LinkedList()
                    list.add(MessageModel(message.content, message.type, hour, date))
                    result.put(date, list)
                }
            }
        }
        return result
    }
}