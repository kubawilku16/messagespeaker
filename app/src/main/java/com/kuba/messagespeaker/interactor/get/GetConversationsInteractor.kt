package com.kuba.messagespeaker.interactor.get

import com.kuba.entities.ContactRealm
import com.kuba.entities.ConversationRealm
import com.kuba.messagespeaker.model.ConversationShortModel
import com.kuba.provider.RealmProvider
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GetConversationsInteractor {
    fun execute(): Observable<List<ConversationShortModel>> =
            Observable.defer { getModel() }.compose { upstream ->
                upstream.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
            }

    private fun getModel(): Observable<List<ConversationShortModel>> {
        val conversations: List<ConversationRealm> = RealmProvider.findAll(ConversationRealm::class.java)
        val contacts: List<ContactRealm> = RealmProvider.findAll(ContactRealm::class.java)
        return Observable.just(conversations.sortedByDescending { conversation -> conversation.messages.first().date }.map { conversation ->
            convertToModel(conversation, contacts)
        }.toList().filterNotNull())
    }

    private fun convertToModel(conversation: ConversationRealm, contacts: List<ContactRealm>): ConversationShortModel? {
        val contact: ContactRealm? = contacts.firstOrNull { c -> c.id == conversation.id }
        return if (contact != null) {
            ConversationShortModel(contact.id, contact.name, contact.photoUri)
        } else {
            null
        }
    }
}