package com.kuba.messagespeaker.interactor.synchronize

import com.kuba.entities.ContactRealm
import com.kuba.entities.ConversationRealm
import com.kuba.entities.MessageRealm
import com.kuba.provider.RealmProvider
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SaveNewMessageInteractor(private val date: Long,
                               private var from: String,
                               private val body: String) {
    fun execute(): Completable =
            Completable.fromCallable { saveMessage() }
                    .compose { upstream ->
                        upstream.subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                    }

    private fun saveMessage() {
        val message = MessageRealm(body, 0, date)
        val contacts: List<ContactRealm> = RealmProvider.findAll(ContactRealm::class.java)
        var contact: ContactRealm? = contacts.firstOrNull { c -> c.number == from }
        if (contact == null) {
            contact = ContactRealm(from, from, from, "")
            RealmProvider.insertOrUpdate(contact)
        }
        var conversation: ConversationRealm? = RealmProvider.findById(ConversationRealm::class.java, "id", contact.id)
        if (conversation == null) {
            conversation = ConversationRealm(contact.id)
        }
        conversation.messages.add(0, message)
        RealmProvider.insertOrUpdate(conversation)
    }
}