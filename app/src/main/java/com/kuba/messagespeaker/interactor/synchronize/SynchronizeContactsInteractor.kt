package com.kuba.messagespeaker.interactor.synchronize

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.kuba.entities.ContactRealm
import com.kuba.provider.RealmProvider
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import android.provider.ContactsContract.PhoneLookup
import android.R.attr.phoneNumber




class SynchronizeContactsInteractor(private val context: Context) {
    companion object {
        private val PHONE_NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER
        private val PHONE_ID = ContactsContract.CommonDataKinds.Phone._ID
        private val PHONE_NAME = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
    }

    fun execute(): Completable =
            Completable.fromCallable { saveToRealm(getCursor()) }
                    .compose { upstream ->
                        upstream.subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                    }


    private fun getCursor(): Cursor? {
        return context
                .contentResolver
                ?.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, arrayOf(PHONE_ID, PHONE_NAME, PHONE_NUMBER)
                        , null, null, PHONE_NAME + " ASC")
    }

    private fun saveToRealm(cursor: Cursor?) {
        RealmProvider.insertOrUpdate(getContacts(cursor))
    }

    private fun getContacts(cursor: Cursor?): List<ContactRealm> {
        val contacts = ArrayList<ContactRealm>()
        if (cursor != null) {
            val phoneNameIndex = cursor.getColumnIndex(PHONE_NAME)
            val phoneNumberIndex = cursor.getColumnIndex(PHONE_NUMBER)
            val phoneIdIndex = cursor.getColumnIndex(PHONE_ID)
            while (cursor.moveToNext()) {
                var number: String = cursor.getString(phoneNumberIndex)
                var photo: String = getPhoto(number)?.toString() ?: ""
                try {
                    number = PhoneNumberUtil.getInstance().parse(number, Locale.getDefault().country).nationalNumber.toString()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                contacts.add(ContactRealm(cursor.getString(phoneIdIndex), cursor.getString(phoneNameIndex), number, photo))
            }
            cursor.close()
        }
        return contacts
    }

    private fun getPhoto(number: String): Uri? {
        val uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number))
        var cursor = context.contentResolver.query(uri,
                arrayOf(PhoneLookup.DISPLAY_NAME, PhoneLookup._ID), null, null, null)
        var contactId : Long = 0L
        if (cursor.moveToFirst()) {
            do {
                contactId = cursor.getLong(cursor
                        .getColumnIndex(PhoneLookup._ID))
            } while (cursor.moveToNext())
        }
        cursor = context.contentResolver
                .query(ContactsContract.Data.CONTENT_URI, null,
                        ContactsContract.Data.CONTACT_ID
                                + "="
                                + contactId
                                + " AND "
                                + ContactsContract.Data.MIMETYPE
                                + "='"
                                + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                                + "'", null, null)
        return try {
            if ((cursor != null && !cursor.moveToFirst()) || cursor == null) {
                null
            } else {
                Uri.withAppendedPath(ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId),
                        ContactsContract.Contacts.Photo.CONTENT_DIRECTORY)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            null
        } finally {
            cursor.close()
        }
    }
}