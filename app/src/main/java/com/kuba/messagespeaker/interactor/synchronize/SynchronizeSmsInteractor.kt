package com.kuba.messagespeaker.interactor.synchronize

import android.content.Context
import android.database.Cursor
import android.provider.Telephony
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.kuba.entities.ContactRealm
import com.kuba.entities.ConversationRealm
import com.kuba.entities.MessageRealm
import com.kuba.provider.RealmProvider
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.RealmList
import java.util.*
import kotlin.collections.ArrayList

class SynchronizeSmsInteractor(private val context: Context) {
    fun execute(): Completable = Completable.fromCallable { saveToRealm(getCursor()) }
            .compose { upstream ->
                upstream
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
            }

    private fun getCursor() =
            context.contentResolver?.query(Telephony.Sms.CONTENT_URI, null, null, null, null)


    private fun saveToRealm(cursor: Cursor?) {
        if (cursor != null && cursor.count > 0)
            RealmProvider.insertOrUpdate(getConversations(cursor))
    }

    private fun getConversations(cursor: Cursor): List<ConversationRealm> {
        val contacts: List<ContactRealm> = RealmProvider.findAll(ContactRealm::class.java)
        val conversations = ArrayList<ConversationRealm>()
        while (cursor.moveToNext()) {
            var number = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.ADDRESS))
            val message = MessageRealm(content = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.BODY))
                    , type = cursor.getInt(cursor.getColumnIndex(Telephony.Sms.TYPE))
                    , date = cursor.getLong(cursor.getColumnIndexOrThrow(Telephony.Sms.DATE)))
            try {
                number = PhoneNumberUtil.getInstance().parse(number, Locale.getDefault().country).nationalNumber.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            var contact: ContactRealm? = contacts.firstOrNull { c -> c.number == number }
            if (contact == null) {
                contact = ContactRealm(number, number, number)
                RealmProvider.insertOrUpdate(contact)
            }
            var conversation: ConversationRealm? = conversations.firstOrNull { c -> c.id == contact!!.id }
            if (conversation == null) {
                conversation = ConversationRealm(contact.id, RealmList(message))
                conversations.add(conversation)
            } else {
                conversations.remove(conversation)
                conversation.messages.add(message)
                conversations.add(conversation)
            }
        }
        return conversations
    }
}