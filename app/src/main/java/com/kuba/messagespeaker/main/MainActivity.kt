package com.kuba.messagespeaker.main

import android.annotation.SuppressLint
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.animation.AccelerateInterpolator
import android.widget.RelativeLayout
import com.kuba.messagespeaker.R
import com.kuba.messagespeaker.base.BaseActivity
import com.kuba.messagespeaker.base.BaseView
import com.kuba.messagespeaker.conversation.ConversationActivity_
import com.kuba.messagespeaker.model.ConversationShortModel
import com.kuba.messagespeaker.prefs.SynchronizeResult_
import com.kuba.messagespeaker.utils.ScrollListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.no_data_layout.*
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.OnActivityResult
import org.androidannotations.annotations.sharedpreferences.Pref

@SuppressLint("Registered")
@EActivity(R.layout.activity_main)
open class MainActivity : BaseActivity<MainPresenter, MainActivity>(), BaseView {
    @Pref
    lateinit var syncResult: SynchronizeResult_

    private var adapter: MainConversationsAdapter? = null

    override fun createPresenter(): MainPresenter = MainPresenter()

    @AfterViews
    fun onInit() {
        presenter?.bind(this)
        presenter?.onInit()
    }

    @Click(R.id.main_start_sync)
    fun onSyncClick() {
        presenter?.onSyncClick()
    }

    fun needToSync(): Boolean = syncResult.needToSync().getOr(true)

    fun showSyncing() {
        main_no_content_view.visibility = INVISIBLE
        main_syncing_view.visibility = VISIBLE
    }

    fun initRecycler() {
        main_recycler_view.addOnScrollListener(ScrollListener({ presenter?.onShow() }, { presenter?.onHide() }))
        main_recycler_view.layoutManager = LinearLayoutManager(applicationContext)
        main_recycler_view.adapter = adapter
    }

    fun initAdapter() {
        adapter = MainConversationsAdapter({ id, name  -> presenter?.onItemClick(id, name) })
    }

    fun showContentView() {
        main_syncing_view.visibility = INVISIBLE
        main_content_view.visibility = VISIBLE
    }

    fun refreshData(result: List<ConversationShortModel>) {
        adapter?.loadData(result)
    }

    fun hideButtons() {
        val margin: Int = (main_add_button.layoutParams as RelativeLayout.LayoutParams).marginEnd
        main_add_button.animate().translationX(main_add_button.width.toFloat() + margin).setInterpolator(AccelerateInterpolator(2.0f)).start()
        main_search_button.animate().translationX(main_add_button.width.toFloat() + margin).setInterpolator(AccelerateInterpolator(2.0f)).start()
    }

    fun showButtons() {
        main_add_button.animate().translationX(0.0f).setInterpolator(AccelerateInterpolator(2.0f)).start()
        main_search_button.animate().translationX(0.0f).setInterpolator(AccelerateInterpolator(2.0f)).start()
    }

    fun startConversationActivity(id: String, name: String) {
        ConversationActivity_.intent(this).id(id).title(name).startForResult(1234)
    }

    @OnActivityResult(1234)
    fun onResult(@OnActivityResult.Extra(value = "id") value: String) {
        if (!TextUtils.isEmpty(value)) {
            adapter?.moveItemById(value)
            main_recycler_view.scrollToPosition(0)
        }
    }
}