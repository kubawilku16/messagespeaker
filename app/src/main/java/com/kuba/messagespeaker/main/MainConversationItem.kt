package com.kuba.messagespeaker.main

import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.kuba.messagespeaker.R
import com.kuba.messagespeaker.base.recyclerview.BaseRVItem
import com.kuba.messagespeaker.model.ConversationShortModel
import com.kuba.messagespeaker.utils.GlideApp
import de.hdodenhof.circleimageview.CircleImageView
import org.jetbrains.anko.find

class MainConversationItem(private var model: ConversationShortModel,
                           private var itemClick: (String, String) -> Unit) : BaseRVItem() {
    override fun getViewType(): Int = R.layout.item_conversation_short

    override fun getViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(getViewType(), parent, false))

    override fun bind(holder: RecyclerView.ViewHolder) {
        (holder as ViewHolder)
        holder.nameText.text = model.name
        holder.itemView.setOnClickListener { itemClick(model.id, model.name) }
        GlideApp.with(holder.itemView.context).load(Uri.parse(model.photoUri)).placeholder(R.drawable.avatar_placeholder).into(holder.avatar)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val avatar: CircleImageView = itemView.find(R.id.item_conversation_short_avatar)
        val nameText: TextView = itemView.find(R.id.item_conversation_short_name)
    }
}