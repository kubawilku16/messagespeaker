package com.kuba.messagespeaker.main

import com.kuba.messagespeaker.base.recyclerview.BaseRVAdapter
import com.kuba.messagespeaker.model.ConversationShortModel

class MainConversationsAdapter(private val itemClick: (String, String) -> Unit) : BaseRVAdapter() {
    private var models: MutableList<ConversationShortModel>? = null

    fun loadData(models: List<ConversationShortModel>) {
        this.models = models.toMutableList()
        models.forEach { model -> addItem(MainConversationItem(model, itemClick)) }
        notifyDataSetChanged()
    }

    fun moveItemById(id: String) {
        val model: ConversationShortModel? = models?.first { model -> model.id == id }
        if (model != null) {
            val position: Int? = models?.indexOf(model)
            if(position == 0) {
                return

            }
            models?.remove(model)
            models?.add(0, model)
            notifyItemMoved(position ?: 0, 0)
        }
    }
}