package com.kuba.messagespeaker.main

import android.util.Log
import com.kuba.messagespeaker.base.BasePresenter
import com.kuba.messagespeaker.interactor.get.GetConversationsInteractor
import com.kuba.messagespeaker.interactor.synchronize.SynchronizeContactsInteractor
import com.kuba.messagespeaker.interactor.synchronize.SynchronizeSmsInteractor
import com.kuba.messagespeaker.model.ConversationShortModel
import org.jetbrains.anko.onUiThread

class MainPresenter : BasePresenter<MainActivity>() {
    fun onInit() {
        view?.initAdapter()
        if (view?.needToSync() != true) {
            view?.showSyncing()
            getData()
        }
    }

    private fun getData() {
        GetConversationsInteractor().execute()
                .subscribe({ result -> onSuccessGetData(result) }, { throwable -> onGetDataError(throwable) })
    }

    fun onSyncClick() {
        view?.showSyncing()
        SynchronizeContactsInteractor(view!!.applicationContext).execute()
                .andThen(SynchronizeSmsInteractor(view!!.applicationContext).execute())
                .andThen(GetConversationsInteractor().execute())
                .subscribe({ result -> onSuccessGetData(result) }, { throwable -> onSynchronizeDataError(throwable) })
    }

    private fun onSuccessGetData(result: List<ConversationShortModel>) {
        view?.syncResult!!.edit().needToSync().put(false).apply()
        view?.onUiThread {
            view?.initRecycler()
            view?.showContentView()
            view?.refreshData(result)
        }
    }

    private fun onGetDataError(throwable: Throwable) {
        Log.e(javaClass.simpleName, throwable.message, throwable)
    }

    private fun onSynchronizeDataError(throwable: Throwable) {
        Log.e(javaClass.simpleName, throwable.message, throwable)
    }

    fun onHide() = view?.hideButtons()

    fun onShow() = view?.showButtons()

    fun onItemClick(id: String, name: String) = view?.startConversationActivity(id, name)
}