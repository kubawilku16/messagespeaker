package com.kuba.messagespeaker.model

data class ConversationModel(val id: String, val name: String,
                             var days: MutableList<String>,
                             var messagesPerDay: MutableMap<String, MutableList<MessageModel>>,
                             var photoUri: String,
                             var phoneNumber: String)