package com.kuba.messagespeaker.model

data class ConversationShortModel(val id : String,
                                  val name: String,
                                  val photoUri: String)