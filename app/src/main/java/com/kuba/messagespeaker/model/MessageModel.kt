package com.kuba.messagespeaker.model

data class MessageModel(val content: String, val type : Int, val hour: String, val date: String)