package com.kuba.messagespeaker.observers

import android.content.ContentResolver
import android.database.ContentObserver
import android.database.Cursor
import android.net.Uri
import android.os.Handler
import android.provider.Telephony
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.kuba.entities.ContactRealm
import com.kuba.entities.ConversationRealm
import com.kuba.entities.MessageRealm
import com.kuba.provider.RealmProvider
import io.realm.RealmList
import java.util.*

class OutgoingMessagesObserver(handler: Handler, private var contentResolver: ContentResolver) : ContentObserver(handler) {
    override fun onChange(selfChange: Boolean) {
        val cursor: Cursor = contentResolver.query(Uri.parse("content://sms/sent"), null, null, null, null)
        cursor.moveToNext()
        var number: String = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.ADDRESS))
        try {
            number = PhoneNumberUtil.getInstance().parse(number, Locale.getDefault().country).nationalNumber.toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val message = MessageRealm(cursor.getString(cursor.getColumnIndex(Telephony.Sms.BODY)), 2, cursor.getLong(cursor.getColumnIndexOrThrow(Telephony.Sms.DATE)))
        var contact: ContactRealm? = RealmProvider.findContactById(number)
        if (contact == null) {
            contact = ContactRealm(number, number, number, "")
            RealmProvider.insertOrUpdate(contact)
        }
        var conversation: ConversationRealm? = RealmProvider.findById(ConversationRealm::class.java, "id", contact.id)
        if (conversation == null) {
            conversation = ConversationRealm(contact.id, RealmList(message))
        } else if (conversation.messages.firstOrNull { mes -> mes.date == message.date } == null) {
            conversation.messages.add(0, message)
        }
        RealmProvider.insertOrUpdate(conversation)
        cursor.close()
        super.onChange(selfChange)
    }
}