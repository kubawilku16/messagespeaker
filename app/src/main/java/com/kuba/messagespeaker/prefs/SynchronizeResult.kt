package com.kuba.messagespeaker.prefs

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean
import org.androidannotations.annotations.sharedpreferences.SharedPref

@SharedPref(SharedPref.Scope.APPLICATION_DEFAULT)
interface SynchronizeResult {
    @DefaultBoolean(true)
    fun needToSync(): Boolean
}