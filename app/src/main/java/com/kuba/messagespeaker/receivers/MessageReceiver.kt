package com.kuba.messagespeaker.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Telephony
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.kuba.messagespeaker.interactor.synchronize.SaveNewMessageInteractor
import java.util.*

class MessageReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Telephony.Sms.Intents.getMessagesFromIntent(intent)
                .forEach { smsMessage ->
                    SaveNewMessageInteractor(System.currentTimeMillis(), getNumber(smsMessage.originatingAddress), smsMessage.messageBody)
                            .execute()
                            .subscribe()
                }
    }

    private fun getNumber(number: String): String =
            try {
                PhoneNumberUtil.getInstance().parse(number, Locale.getDefault().country).nationalNumber.toString()
            } catch (e: Exception) {
                e.printStackTrace()
                number
            }
}