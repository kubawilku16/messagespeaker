package com.kuba.messagespeaker.recognizer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import java.util.*

class Recognizer(val context: Context,
                 private val onStateChange: (Boolean) -> Unit,
                 private val onResult: (String?) -> Unit) {
    private var speechRecognizer: SpeechRecognizer? = null
    private var isListening: Boolean = false

    fun create(): Recognizer {
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context)
        speechRecognizer?.setRecognitionListener(SpeechListener(context, { isListening -> onStateChanged(isListening) }, { bundle -> onResult(bundle) }))
        return this
    }

    private fun onStateChanged(listening: Boolean) {
        this.isListening = listening
        onStateChange(listening)
    }

    private fun onResult(bundle: Bundle?) {
        onResult(bundle?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)?.get(0))
    }

    private fun start() {
        speechRecognizer?.startListening(Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
                .putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, 10000)
                .putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
                .putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault()))
    }

    private fun stop() {
        speechRecognizer?.cancel()
        onStateChange(false)
    }

    fun onSpeakClick() {
        if (isListening) {
            stop()
        } else {
            start()
        }
    }
}