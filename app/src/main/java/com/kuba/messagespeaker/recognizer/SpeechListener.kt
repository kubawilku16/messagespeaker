package com.kuba.messagespeaker.recognizer

import android.content.Context
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.SpeechRecognizer
import android.widget.Toast
import com.kuba.messagespeaker.R

class SpeechListener(val context: Context,
                     private val isListening: (Boolean) -> Unit,
                     private val onResult: (Bundle?) -> Unit) : RecognitionListener {
    override fun onReadyForSpeech(p0: Bundle?) {
        isListening(true)
    }

    override fun onRmsChanged(p0: Float) {}

    override fun onBufferReceived(p0: ByteArray?) {}

    override fun onPartialResults(p0: Bundle?) {
        onResult(p0)
    }

    override fun onEvent(p0: Int, p1: Bundle?) {}

    override fun onBeginningOfSpeech() {
    }

    override fun onEndOfSpeech() {}

    override fun onError(errorCode: Int) {
        when (errorCode) {
            in arrayOf(SpeechRecognizer.ERROR_SERVER,
                    SpeechRecognizer.ERROR_NETWORK,
                    SpeechRecognizer.ERROR_NETWORK_TIMEOUT) ->
                Toast.makeText(context, context.getString(R.string.no_internet_error), Toast.LENGTH_SHORT).show()
        }
        isListening(false)
    }

    override fun onResults(p0: Bundle?) {
        isListening(false)
        onResult(p0)
    }
}