package com.kuba.messagespeaker.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Handler
import android.support.v4.app.ActivityCompat
import com.kuba.messagespeaker.R
import com.kuba.messagespeaker.base.BaseActivity
import com.kuba.messagespeaker.base.BaseView
import com.kuba.messagespeaker.main.MainActivity_
import com.kuba.messagespeaker.utils.PermissionUtils
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity
import org.jetbrains.anko.alert

@SuppressLint("Registered")
@EActivity(R.layout.splash_activity)
open class SplashActivity : BaseActivity<SplashPresenter, SplashActivity>(), BaseView {
    override fun createPresenter(): SplashPresenter = SplashPresenter()

    companion object {
        private val PERMISSIONS_ALL = 123
    }

    @AfterViews
    fun onInit() {
        presenter?.bind(this)
        presenter?.onInit()
    }

    fun askForPermissions(): Any = if (!PermissionUtils.hasPermissions(applicationContext)) {
        ActivityCompat.requestPermissions(this, PermissionUtils.permissions, PERMISSIONS_ALL)
    } else {
        startMainActivity()
    }

    fun startMainActivity() = MainActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK).start()!!

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        presenter?.onRequestPermissionResult(grantResults)
    }

    fun showPermissionsError() {
        alert(getString(R.string.permissions_error), getString(R.string.error)) {
            positiveButton(getString(R.string.again)) { askForPermissions() }
            negativeButton(getString(R.string.finish)) { Handler().postDelayed({finish()}, 1000)}
            onCancel { Handler().postDelayed({finish()}, 1000)}
        }.show()
    }
}