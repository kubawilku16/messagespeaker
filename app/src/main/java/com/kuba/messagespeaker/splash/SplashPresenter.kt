package com.kuba.messagespeaker.splash

import android.os.Handler
import com.kuba.messagespeaker.base.BasePresenter

class SplashPresenter : BasePresenter<SplashActivity>() {
    fun onInit() {
        Handler().postDelayed({ view?.askForPermissions() }, 1000)
    }

    fun onRequestPermissionResult(grantResults: IntArray) {
        if(grantResults.any { value -> value != 0 }) {
            view?.showPermissionsError()
        } else {
            view?.startMainActivity()
        }
    }
}