package com.kuba.messagespeaker.utils

class MSTimeUtils {
    companion object {
        val HEADER_DATE_PATTERN = "dd MMMM yyyy"
        val MESSAGE_TIME_PATTERN = "HH:mm"
    }
}