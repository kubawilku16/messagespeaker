package com.kuba.messagespeaker.utils

import android.Manifest.permission.*
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat

class PermissionUtils {
    companion object {
        val permissions = arrayOf(READ_CONTACTS, RECORD_AUDIO, READ_SMS, SEND_SMS, RECEIVE_SMS)

        fun hasPermissions(context: Context?): Boolean =
                !(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null) ||
                        !permissions
                                .any { permission -> ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED }
    }
}