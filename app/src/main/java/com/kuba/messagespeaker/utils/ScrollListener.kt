package com.kuba.messagespeaker.utils

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView


class ScrollListener(private val onShow: () -> Unit, private val onHide: () -> Unit) : RecyclerView.OnScrollListener() {
    private val offset = 20
    private var scrolledDistance = 0
    private var controlsVisible = true

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val firstVisibleItem = (recyclerView?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        if (firstVisibleItem == 0) {
            if (!controlsVisible) {
                onShow()
                controlsVisible = true
            }
        } else {
            if (scrolledDistance > offset && controlsVisible) {
                onHide()
                controlsVisible = false
                scrolledDistance = 0
            } else if (scrolledDistance < -offset && !controlsVisible) {
                onShow()
                controlsVisible = true
                scrolledDistance = 0
            }
        }
        if (controlsVisible && dy > 0 || !controlsVisible && dy < 0) {
            scrolledDistance += dy
        }
    }
}