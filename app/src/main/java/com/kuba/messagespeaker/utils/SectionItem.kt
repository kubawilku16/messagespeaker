package com.kuba.messagespeaker.utils

interface SectionItem {
    fun getSectionIndex() : Int
    fun incrementSection(count: Int)
}