package com.kuba.messagespeaker.utils

import android.text.Editable
import android.text.TextWatcher

class TextChangeListener(private var onTextChange: (String) -> Unit) : TextWatcher {
    override fun afterTextChanged(p0: Editable?) {
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        onTextChange(p0.toString())
    }
}