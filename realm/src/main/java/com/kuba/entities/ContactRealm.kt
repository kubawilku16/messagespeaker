package com.kuba.entities

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ContactRealm(@PrimaryKey var id: String = "",
                        var name: String = "",
                        var number: String = "",
                        var photoUri: String = "") : RealmObject()