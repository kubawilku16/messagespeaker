package com.kuba.entities

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ConversationRealm(@PrimaryKey var id: String = "",
                             var messages: RealmList<MessageRealm> = RealmList()) : RealmObject()