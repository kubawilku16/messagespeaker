package com.kuba.entities

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class MessageRealm(var content: String = "",
                        var type: Int = 0,
                        @PrimaryKey
                        var date: Long = 0) : RealmObject()