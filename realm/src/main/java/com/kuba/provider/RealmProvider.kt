package com.kuba.provider

import android.content.Context
import com.kuba.entities.ContactRealm
import io.realm.Realm
import io.realm.RealmObject
import java.util.*

class RealmProvider {
    companion object {
        private var applicationContext: Context? = null
        fun init(context: Context) {
            Realm.init(context)
            applicationContext = context
            RealmProvider()
        }

        @Suppress("UNCHECKED_CAST")
        fun <T : RealmObject> findById(clazz: Class<out RealmObject>, keyName: String, key: String): T? {
            val realm: Realm = Realm.getDefaultInstance()
            try {
                if (realm.isClosed) {
                    Realm.init(applicationContext!!)
                }
                val result = realm.where(clazz).equalTo(keyName, key).findFirst()
                return if (result == null) {
                    null
                } else {
                    realm.copyFromRealm(result) as T
                }
            } finally {
                if (!realm.isClosed) {
                    realm.close()
                }
            }
        }

        fun <T : RealmObject> insertOrUpdate(`object`: T) {
            val realm = Realm.getDefaultInstance()
            try {
                if (realm.isClosed) {
                    Realm.init(applicationContext!!)
                }
                realm.beginTransaction()
                realm.insertOrUpdate(`object`)
                realm.commitTransaction()
            } catch (e: Exception) {
                if (realm.isInTransaction) {
                    realm.cancelTransaction()
                }
                e.printStackTrace()
            } finally {
                if (!realm.isClosed) {
                    realm.close()
                }
            }
        }

        fun <T : RealmObject> insertOrUpdate(objects: List<T>) {
            val realm = Realm.getDefaultInstance()
            try {
                if (realm.isClosed) {
                    Realm.init(applicationContext!!)
                }
                realm.beginTransaction()
                realm.insertOrUpdate(objects)
                realm.commitTransaction()
            } catch (e: Exception) {
                if (realm.isInTransaction) {
                    realm.cancelTransaction()
                }
                e.printStackTrace()
            } finally {
                if (!realm.isClosed) {
                    realm.close()
                }
            }
        }

        @Suppress("UNCHECKED_CAST")
        fun <T : RealmObject> findAll(clazz: Class<out RealmObject>): List<T> {
            val realm = Realm.getDefaultInstance()
            try {
                if (realm.isClosed) {
                    Realm.init(applicationContext!!)
                }
                val results = realm.where(clazz).findAll()
                return if (results != null) {
                    realm.copyFromRealm(results) as List<T>
                } else {
                    ArrayList()
                }
            } finally {
                if (!realm.isClosed) {
                    realm.close()
                }
            }
        }

        fun delete(clazz: Class<out RealmObject>, keyName: String, id: String) {
            val realm = Realm.getDefaultInstance()
            try {
                if (realm.isClosed) {
                    Realm.init(applicationContext!!)
                }
                realm.beginTransaction()
                val result = realm.where(clazz).equalTo(keyName, id).findFirst()
                result?.deleteFromRealm()
                realm.commitTransaction()
            } catch (e: Exception) {
                if (realm.isInTransaction) {
                    realm.cancelTransaction()
                }
                e.printStackTrace()
            } finally {
                if (!realm.isClosed) {
                    realm.close()
                }
            }
        }

        fun delete(clazz: Class<out RealmObject>, keyName: String, id: Long?) {
            val realm = Realm.getDefaultInstance()
            try {
                if (realm.isClosed) {
                    Realm.init(applicationContext!!)
                }
                realm.beginTransaction()
                val result = realm.where(clazz).equalTo(keyName, id).findFirst()
                result?.deleteFromRealm()
                realm.commitTransaction()
            } catch (e: Exception) {
                if (realm.isInTransaction) {
                    realm.cancelTransaction()
                }
                e.printStackTrace()
            } finally {
                if (!realm.isClosed) {
                    realm.close()
                }
            }
        }

        fun deleteAll(T: Class<out RealmObject>) {
            val realm = Realm.getDefaultInstance()
            try {
                if (realm.isClosed) {
                    Realm.init(applicationContext!!)
                }
                realm.beginTransaction()
                realm.delete(T)
                realm.commitTransaction()
            } catch (e: Exception) {
                if (realm.isInTransaction) {
                    realm.cancelTransaction()
                }
                e.printStackTrace()
            } finally {
                if (!realm.isClosed) {
                    realm.close()
                }
            }
        }

        fun findContactById(number: String): ContactRealm? {
            val realm: Realm = Realm.getDefaultInstance()
            try {
                if (realm.isClosed) {
                    Realm.init(applicationContext!!)
                }
                val result = realm.where(ContactRealm::class.java).equalTo("number", number).findFirst()
                return if (result == null) {
                    null
                } else {
                    realm.copyFromRealm(result) as ContactRealm
                }
            } finally {
                if (!realm.isClosed) {
                    realm.close()
                }
            }
        }
    }
}